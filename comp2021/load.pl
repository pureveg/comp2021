#!/usr/bin/perl
use 5.010;
use lib qw/./;
use AnyEvent;
use AnyEvent::HproseHttpClient;
use Sys::Statistics::Linux;
use Data::Dumper;
use POSIX;

our $phone = $ARGV[0];
my $hostname = `hostname`;

sub cpu
{
	$SLEEPTIME=5;  
	 
	if (-e "/tmp/stat") {  
		unlink "/tmp/stat";  
	}  
	open (JIFF_TMP, ">>/tmp/stat") || die "Can't open /proc/stat file!\n";  
	open (JIFF, "/proc/stat") || die "Can't open /proc/stat file!\n";  
	@jiff_0=<JIFF>;  
	print JIFF_TMP $jiff_0[0] ;  
	close (JIFF);  
	 
	sleep $SLEEPTIME;  
	 
	open (JIFF, "/proc/stat") || die "Can't open /proc/stat file!\n";  
	@jiff_1=<JIFF>;  
	print JIFF_TMP $jiff_1[0];  
	close (JIFF);  
	close (JIFF_TMP);  
	 
	@USER=`awk '{print \$2}' "/tmp/stat"`;  
	@NICE=`awk '{print \$3}' "/tmp/stat"`;  
	@SYSTEM=`awk '{print \$4}' "/tmp/stat"`;  
	@IDLE=`awk '{print \$5}' "/tmp/stat"`;  
	@IOWAIT=`awk '{print \$6}' "/tmp/stat"`;  
	@IRQ=`awk '{print \$7}' "/tmp/stat"`;  
	@SOFTIRQ=`awk '{print \$8}' "/tmp/stat"`;  
	 
	$JIFF_0=$USER[0]+$NICE[0]+$SYSTEM[0]+$IDLE[0]+$IOWAIT[0]+$IRQ[0]+$SOFTIRQ[0];  
	$JIFF_1=$USER[1]+$NICE[1]+$SYSTEM[1]+$IDLE[1]+$IOWAIT[1]+$IRQ[1]+$SOFTIRQ[1];  
	 
	if ($JIFF_0 == $JIFF_1) {
		return 0;
	}
	$SYS_IDLE=($IDLE[0]-$IDLE[1]) / ($JIFF_0-$JIFF_1) * 100;  
	$SYS_USAGE=100 - $SYS_IDLE;  
	 
	return $SYS_USAGE;
}

sub mem 
{
	local $lxs = Sys::Statistics::Linux->new(
		memstats  => 1,
	);
	local $mem = $lxs->get()->memstats;
	local $total = $mem->{memtotal};
	local $free  = $mem->{realfree};
	return ($total - $free)/$total;
}

sub disk 
{
	local $lxs = Sys::Statistics::Linux->new(
		diskusage => 1,
	);
	local @disk = $lxs->get()->diskusage;
	local $total = 0;
	local $free  = 0;
	foreach my $index (@disk) {
		$total += $lxs->get()->diskusage->{$index}->{total};
		$free  += $lxs->get()->diskusage->{$index}->{free};
	}
	return ($total - $free)/$total;
}

# sub dumpStruct
# {
# 	local $lxs = Sys::Statistics::Linux->new(
# 		sysinfo   => 1,
# 		diskusage => 1,
# 	);
# 	my $stat = $lxs->get;
# 	print Dumper($stat);
# }

# print dumpStruct();
#
while (1) {
	local $cv = AE::cv;
	local $client = AnyEvent::HproseHttpClient->new('http://server.pureveg.pw:9999/');
	local $timestamp = time();

	local $cpuData  = 	{'timestamp' => $timestamp, 'usage' => cpu()};
	local $memData  = 	{'timestamp' => $timestamp, 'usage' => mem()};
	local $diskData = 	{'timestamp' => $timestamp, 'usage' => disk()};

	local $data = {
		'cpuData'  => $cpuData, 
		'memData'  => $memData, 
		'diskData' => $diskData
	};
	
	$client->setData($hostname, $phone, $data, sub{say @_; $cv->send;}); 
	$cv->recv;
	sleep 1;
}

