package main

import (
	"fmt"
	"log"
	"net/http"
	"reflect"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/pureveg/uniltc_utils"

	"github.com/hprose/hprose-go/hprose"
)

type data struct {
	Timestamp int64
	Usage     string
}

type serverData struct {
	CpuData  data
	MemData  data
	DiskData data
}

// hostname -> serverData
type servers struct {
	d map[string][]serverData
	sync.Mutex
}

func newServers() *servers {
	s := &servers{
		d: make(map[string][]serverData, 0),
	}
	go s.clear()
	return s
}

const (
	interval  = 60 * 60
	limitCpu  = 70.0
	limitMem  = 80.0
	limitDisk = 80.0
)

func (s *servers) clear() {
	for {
		func() {
			s.Lock()
			defer s.Unlock()
			for key, val := range s.d {
				lastIndex := len(val) - 1
				if time.Now().Unix()-val[lastIndex].CpuData.Timestamp > interval {
					delete(s.d, key)
				}
			}
		}()
		time.Sleep(5 * time.Second)
	}
}

func nexmo(phone string, hostname string, val serverData) {
	if phone == "" {
		return
	}
	cpuUsage, err := strconv.ParseFloat(val.CpuData.Usage, 64)
	if err != nil {
		log.Println(err)
		return
	}
	memUsage, err := strconv.ParseFloat(val.MemData.Usage, 64)
	if err != nil {
		log.Println(err)
		return
	}
	diskUsage, err := strconv.ParseFloat(val.MemData.Usage, 64)
	if err != nil {
		log.Println(err)
		return
	}
	if cpuUsage <= limitCpu && memUsage <= limitMem && diskUsage <= limitDisk {
		return
	}
	text := fmt.Sprintf("Something is wrong with your server with hostname %s", hostname)
	err = uniltc_utils.Call("852"+phone, text, "", "", "2")
	if err != nil {
		log.Println(err)
	}
}

func (s *servers) Set(hostname string, phone string, val serverData) {
	s.Lock()
	defer s.Unlock()
	log.Printf("hostname is %s phone is %s and val is %v\n", hostname, phone, val)
	s.d[hostname] = append(s.d[hostname], val)
	go nexmo(phone, hostname, val)
}

func (s *servers) Get() interface{} {
	s.Lock()
	defer s.Unlock()
	return s.d
}

type myServiceEvent struct{}

func (myServiceEvent) OnBeforeInvoke(name string, args []reflect.Value, byref bool, context interface{}) {
}

func (myServiceEvent) OnAfterInvoke(name string, args []reflect.Value, byref bool, result []reflect.Value, context interface{}) {
}

func (myServiceEvent) OnSendError(err error, context interface{}) {
	fmt.Println(err)
}

var datas = newServers()

func main() {
	server := hprose.NewHttpService()
	server.ServiceEvent = myServiceEvent{}
	server.DebugEnabled = true
	// for js
	server.AddFunction("getData", datas.Get)
	// for client
	server.AddFunction("setData", datas.Set)
	// for testing
	err := http.ListenAndServe(":9999", server)
	if err != nil {
		log.Println(err)
	}
}
